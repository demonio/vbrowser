#ifndef VBROWSER_H
#define VBROWSER_H

#include <QMainWindow>
#include <QtWebKitWidgets/QWebView>
#include <iostream>
#include <vector>
using namespace std;
namespace Ui {
class vbrowser;
}

class vbrowser : public QMainWindow
{
    Q_OBJECT

public:
    explicit vbrowser(QWidget *parent = 0);
    ~vbrowser();

private slots:
    void on_url_returnPressed();
    void update_history(QUrl);

    void on_lasturl_clicked();

    void on_nexturl_clicked();

    void on_webView_urlChanged(const QUrl &arg1);

    void on_Domu_clicked();

private:
    Ui::vbrowser *ui;
    unsigned int history_page=0;
    vector< QUrl > history_url;
};

#endif // VBROWSER_H
