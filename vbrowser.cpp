#include "vbrowser.h"
#include "ui_vbrowser.h"

vbrowser::vbrowser(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::vbrowser)
{
    ui->setupUi(this);
}

vbrowser::~vbrowser()
{
    delete ui;
}

void vbrowser::on_url_returnPressed()
{
    string text = this->ui->url->text().toUtf8().constData();
    if(text.find("http://") != string::npos){
        this->ui->webView->setUrl(QUrl(this->ui->url->text().toUtf8().constData()));
        vbrowser::update_history(QUrl(this->ui->url->text().toUtf8().constData()));
    }
    else{
        text = "http://"+text;
        const char* text2 = text.c_str();
        this->ui->webView->setUrl(QUrl(text2));
        //vbrowser::update_history(QUrl(this->ui->url->text().toUtf8().constData()));
    }
}

void vbrowser::update_history(QUrl url){
    while(history_page < history_url.size()){
         history_url.pop_back();
    }
    history_page++;
    history_url.push_back(url);
}

void vbrowser::on_lasturl_clicked()
{
    if(history_page > 1){
        history_page--;
        this->ui->webView->setUrl(history_url[history_page-1]);
        this->ui->url->setText(history_url[history_page-1].toString());
    }
}

void vbrowser::on_nexturl_clicked()
{
    if(history_page < history_url.size()){
        history_page++;
        this->ui->webView->setUrl(history_url[history_page-1]);
        this->ui->url->setText(history_url[history_page-1].toString());
    }
}

void vbrowser::on_webView_urlChanged(const QUrl &arg1)
{
    vbrowser::update_history(arg1);
    this->ui->url->setText(arg1.toString());
}

void vbrowser::on_Domu_clicked()
{
    history_page = 0;
    vbrowser::update_history(QUrl("http://google.com"));
    this->ui->webView->setUrl(QUrl("http://google.com"));
    this->ui->url->setText("http://google.com");
}
