#-------------------------------------------------
#
# Project created by QtCreator 2014-12-13T22:41:01
#
#-------------------------------------------------

#QT       += core gui
QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vbrowser
TEMPLATE = app


SOURCES += main.cpp\
        vbrowser.cpp

HEADERS  += vbrowser.h

FORMS    += vbrowser.ui
